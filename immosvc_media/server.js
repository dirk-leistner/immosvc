/* immosvc_media */
var http = require('http');
var util = require('util');
var express = require('express');
var bodyParser = require('body-parser');
var port = process.env.PORT || 9105;
var app = express();
var mongo = require('mongodb').MongoClient;
var mongoUrl = 'mongodb://localhost:27017/';
var mongoDb = 'immo_media';
var mongoCollection = 'media_entries';
var ObjectID = require('mongodb').ObjectID;

app.use(bodyParser.urlencoded({ extended: false, limit: '20mb' }));
app.use(bodyParser.json({ limit: '20mb' }));

app.use(function (req, res, next) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    next();
});

app.post('/', function (req, res) {

    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var filename = req.body.filename;
    var data = req.body.data;
    var width = req.body.width;
    var height = req.body.height;
    var temporary = req.body.temporary;

    console.log('filename: ' + filename);
    console.log('data: ' + data);
    console.log('width: ' + width);
    console.log('height: ' + height);
    console.log('temporary: ' + temporary);

    if (filename == null || height == null || width == null || data == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        mongo.connect(mongoUrl, { useNewUrlParser: true }, function (err, db) {

            if (err) throw err;

            var dbo = db.db(mongoDb);
            var insertEntry = { filename: filename, data: data, width: width, height: height, temporary: temporary };

            dbo.collection(mongoCollection).insertOne(insertEntry, function (err, result) {

                if (err) throw err;
                console.log('Media successfully inserted');
                console.log('Result: ' + result);
                console.log('ObjectId: ' + insertEntry._id);
                db.close();

                res.statusCode = 200;
                res.json({
                    statusCode: 200,
                    statusMessage: 'Media successfully inserted',
                    data: {
                        "object_id": insertEntry._id
                    }
                });

                return;
            });
        });
    }
});

app.put('/', function (req, res) {

    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var object_id = req.query.object_id;
    var method = req.query.method;

    if (object_id == null || method == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        if (method == 'settempfalse') {

            mongo.connect(mongoUrl, { useNewUrlParser: true }, function (err, db) {

                if (err) throw err;

                var dbo = db.db(mongoDb);

                dbo.collection(mongoCollection).updateOne({ _id: new ObjectID(object_id) }, { $set: { temporary: false } }, function (err, result) {

                    if (err) throw err;

                    db.close();

                    res.statusCode = 200;
                    res.json({
                        statusCode: 200,
                        statusMessage: 'Media successfully updated',
                        data: {
                            "object_id": object_id
                        }
                    });
                });
            });
        }
    }
});

app.delete('/', function (req, res) {

    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var object_id = req.body.object_id;

    if (object_id == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        mongo.connect(mongoUrl, { useNewUrlParser: true }, function (err, db) {

            if (err) throw err;

            var dbo = db.db(mongoDb);
            var deleteQuery = { _id: object_id };

            dbo.collection(mongoCollection).deleteOne(deleteQuery, function (err, result) {

                if (err) throw err;

                db.close();

                res.statusCode = 200;
                res.json({
                    statusCode: 200,
                    statusMessage: 'Media successfully deleted',
                    data: {
                        "object_id": object_id
                    }
                });
            });
        });
    }
});

app.get('/', function (req, res) {

    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var object_id = req.query.object_id;

    console.log('object_id: ', object_id);

    if (object_id == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        mongo.connect(mongoUrl, { useNewUrlParser: true }, function (err, db) {

            if (err) throw err;

            var dbo = db.db(mongoDb);
            dbo.collection(mongoCollection).findOne({ "_id": new ObjectID(object_id) }, function (err, result) {

                if (err) throw err;

                db.close();

                console.log('result: ' + JSON.stringify(result));

                res.statusCode = 200;
                res.json({
                    statusCode: 200,
                    statusMessage: 'Media found',
                    data: {
                        "object_id": result._id,
                        "filename": result.filename,
                        "height": result.height,
                        "width": result.width,
                        "data": result.data
                    }
                });

                return;
            });
        });
    }
});

app.listen(port, function () {

    console.log('Immo media service running at port ' + port + '...');
});
