var http = require('http');
var util = require('util');
var express = require('express');
var bodyParser = require('body-parser');
var port = process.env.PORT || 1336;
var app = express();
var mongo = require('mongodb').MongoClient;
var mongoUrl = 'mongodb://localhost:27017/';
var mongoDb = 'immo_category';
var mongoCollection = 'category_entries';
var ObjectID = require('mongodb').ObjectID;

app.use(bodyParser.urlencoded({ extended: false, limit: '5mb' }));
app.use(bodyParser.json());

app.use(function (req, res, next) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    next();
});

app.get('/', function (req, res) {

    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var type = req.query.type;

    if (type == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        mongo.connect(mongoUrl, { useNewUrlParser: true }, function (err, db) {

            if (err) throw err;

            var dbo = db.db(mongoDb);
            var getQuery = { type: type };

            dbo.collection(mongoCollection).findOne(getQuery, function (err, result) {

                if (err) throw err;

                db.close();

                res.statusCode = 200;
                res.json({
                    statusCode: 200,
                    statusMessage: 'Object found',
                    data: result
                });

                return;
            });
        });
    }
});

app.post('/', function (req, res) {

    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var type = req.body.type;
    var data = req.body.data;

    console.log('type: ' + type);
    console.log('data: ' + JSON.stringify(data));

    if (type == null || data == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        mongo.connect(mongoUrl, { useNewUrlParser: true }, function (err, db) {

            if (err) throw err;

            var dbo = db.db(mongoDb);
            var insertEntry = { type: type, data: data };

            dbo.collection(mongoCollection).insertOne(insertEntry, function (err, result) {

                if (err) throw err;
                console.log('Category successfully inserted');
                console.log('Result: ' + result);
                console.log('ObjectId: ' + insertEntry._id);
                db.close();

                res.statusCode = 200;
                res.json({
                    statusCode: 200,
                    statusMessage: 'Category successfully inserted',
                    data: {
                        "object_id": insertEntry._id
                    }
                });

                return;
            });
        });
    }
});

app.listen(port, function () {

    console.log('Immo object service running at port ' + port + '...');
});
