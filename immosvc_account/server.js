var http = require('http');
var util = require('util');
var express = require('express');
var bodyParser = require('body-parser');
var md5 = require('md5');
var sha1 = require('sha1');
var port = process.env.PORT || 1336;
var app = express();
var mysql = require('mysql');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var executeDbActionCB = function (sql, callback) {

    console.log('executeDbActionCB begin');

    console.log('SQL: ' + sql);

    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'immo_account'
    });

    connection.connect();
    connection.query(sql, function (err, result) {

        connection.end();

        if (err) callback(err, null);
        else callback(null, result);
        if (err) console.log(err.message);

        console.log('DB-Result: ' + JSON.stringify(result));
    });

    console.log('executeDbActionCB end');
}

app.post('/immosvc/account/register', function (req, res) {

    console.log('/immosvc/account/register begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var data = req.body.data;

    console.log('data: ', data);

    jsonData = JSON.parse(data);

    var username = jsonData['username'];
    var password = jsonData['password'];
    var email = jsonData['email'];

    console.log('username: ', username);
    console.log('password: ', password);
    console.log('email: ', email);

    if (username == null || password == null || email == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        //var passwordmd5 = md5(password);
        var passwordsha1 = sha1(password);
        //var activationtoken = md5(Math.random().toString());
        var activationtoken = sha1(Math.random().toString());
        var sql = 'INSERT INTO `account` (username, password, email, activationtoken, status) VALUE (\'' + username + '\',\'' + passwordsha1 + '\',\'' + email + '\',\'' + activationtoken + '\',0)';

        executeDbActionCB(sql, function (err, result) {

            if (err) {
                console.log('ERROR: ' + err);

                res.statusCode = 500;
                res.json({ statusCode: 500, statusMessage: 'Invalid function call' });

                return;
            }
            else {
                console.log('RESULT: ' + JSON.stringify(result));

                res.statusCode = 200;
                res.json({ statusCode: 200, statusMessage: 'User successfully registered' });

                return;
            }
        });
    }

    console.log('/immosvc/account/register end');
});

app.post('/immosvc/account/activate', function (req, res) {

    console.log('/immosvc/account/activate begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var data = req.body.data;

    console.log('data: ', data);

    jsonData = JSON.parse(data);

    var activationtoken = jsonData['activationtoken'];

    console.log('activationtoken: ', activationtoken);

    if (activationtoken == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        var sql = 'UPDATE `account` SET `status` = 1 WHERE `id` > 0 AND `status` = 0 AND `activationtoken` = \'' + activationtoken + '\'';

        executeDbActionCB(sql, function (err, result) {

            if (err) {
                console.log('ERROR: ' + err);

                res.statusCode = 500;
                res.json({ statusCode: 500, statusMessage: 'Invalid function call' });

                return;
            }
            else {
                console.log('RESULT: ' + JSON.stringify(result));

                res.statusCode = 200;
                res.json({ statusCode: 200, statusMessage: 'User successfully activated' });

                return;
            }
        });
    }

    console.log('/immosvc/account/activate end');
});

app.post('/immosvc/account/update', function (req, res) {

    console.log('/immosvc/account/update begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var data = req.body.data;

    console.log('data: ', data);

    jsonData = JSON.parse(data);

    res.statusCode = 500;
    res.json({ statusCode: 500, statusMessage: 'Not implemented' });

    console.log('/immosvc/account/update end');
});

app.post('/immosvc/account/updateEmail', function (req, res) {

    console.log('/immosvc/account/updateEmail begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var data = req.body.data;

    console.log('data: ', data);

    jsonData = JSON.parse(data);

    res.statusCode = 500;
    res.json({ statusCode: 500, statusMessage: 'Not implemented' });

    console.log('/immosvc/account/updateEmail end');
});

app.post('/immosvc/account/updatePassword', function (req, res) {

    console.log('/immosvc/account/updatePassword begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var data = req.body.data;

    console.log('data: ', data);

    jsonData = JSON.parse(data);

    res.statusCode = 500;
    res.json({ statusCode: 500, statusMessage: 'Not implemented' });

    console.log('/immosvc/account/updatePassword end');
});

app.post('/immosvc/account/updateUsername', function (req, res) {

    console.log('/immosvc/account/updateUsername begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var data = req.body.data;

    console.log('data: ', data);

    jsonData = JSON.parse(data);

    res.statusCode = 500;
    res.json({ statusCode: 500, statusMessage: 'Not implemented' });

    console.log('/immosvc/account/updateUsername end');
});

app.post('/immosvc/account/updateStatus', function (req, res) {

    console.log('/immosvc/account/updateStatus begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var data = req.body.data;

    console.log('data: ', data);

    jsonData = JSON.parse(data);

    res.statusCode = 500;
    res.json({ statusCode: 500, statusMessage: 'Not implemented' });

    console.log('/immosvc/account/updateStatus end');
});

app.listen(port, function () {

    console.log('Immo account service running at port ' + port + '...');
});
