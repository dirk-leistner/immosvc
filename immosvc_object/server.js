/* immosvc_object */
var http = require('http');
var util = require('util');
var express = require('express');
var bodyParser = require('body-parser');
var port = process.env.PORT || 9106;
var app = express();
var mongo = require('mongodb').MongoClient;
var mongoUrl = 'mongodb://localhost:27017/';
var mongoDb = 'immo_object';
var mongoCollection = 'object_entries';
var ObjectID = require('mongodb').ObjectID;
var cors = require('cors');

app.use(cors({origin: 'http://localhost:4200'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '1mb' }));
app.use(bodyParser.json({ limit: '1mb' }));

app.use(function (req, res, next) {
    //console.log(req);
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "*");

    next();
});

app.post('/', function (req, res) {
    console.log('post');
    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var data = req.body.data;

    if (data == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return
    }
    else {

        mongo.connect(mongoUrl, { useNewUrlParser: true }, function (err, db) {

            if (err) throw err;

            var dbo = db.db(mongoDb);
            var insertEntry = { data: data };

            dbo.collection(mongoCollection).insertOne(insertEntry, function (err, result) {

                if (err) throw err;

                db.close();

                res.statusCode = 200;
                res.json({
                    statusCode: 200,
                    statusMessage: 'Object successfully inserted',
                    data: {
                        "object_id": insertEntry._id
                    }
                });

                return;
            });
        });
    }
});

app.put('/', function (req, res) {
    console.log('put');
    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var data = req.body['data'];
    var object_id = data['object_id'];

    console.log("data: " + data);
    console.log("object_id: " + data['object_id']);

    if (data == null || data['object_id'] == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        mongo.connect(mongoUrl, { useNewUrlParser: true }, function (err, db) {
            if (err) throw err;

            var dbo = db.db(mongoDb);

            dbo.collection(mongoCollection).replaceOne({ _id: new ObjectID(object_id) }, data, function (err, response) {

                if (err) throw err;

                db.close();

                res.statusCode = 200;
                res.json({
                    statusCode: 200,
                    statusMessage: 'Object successfully updated',
                    data: {
                        "object_id": object_id
                    }
                });
            });
        });
    }
});

app.delete('/', function (req, res) {
    console.log('delete');
    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var object_id = req.body.object_id;

    if (object_id == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        mongo.connect(mongoUrl, { useNewUrlParser: true }, function (err, db) {

            if (err) throw err;

            var dbo = db.db(mongoDb);
            var deleteQuery = { _id: object_id };

            dbo.collection(mongoCollection).deleteOne(deleteQuery, function (err, result) {

                if (err) throw err;

                db.close();

                res.statusCode = 200;
                res.json({
                    statusCode: 200,
                    statusMessage: 'Object successfully deleted',
                    data: {
                        "object_id": object_id
                    }
                });
            });
        });
    }
});

app.get('/', function (req, res) {
    console.log('get');
    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var object_id = req.query.object_id;

    if (object_id == null) {

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Missing parameters' });

        return;
    }
    else {

        mongo.connect(mongoUrl, { useNewUrlParser: true }, function (err, db) {

            if (err) throw err;

            var dbo = db.db(mongoDb);
            var getQuery = { _id: ObjectID(object_id) };
            dbo.collection(mongoCollection).findOne(getQuery, function (err, result) {

                if (err) throw err;

                db.close();

                res.statusCode = 200;
                res.json({
                    statusCode: 200,
                    statusMessage: 'Object found',
                    data: {
                        "object_id": result._id,
                        "data": data
                    }
                });

                return;
            });
        });
    }
});

app.listen(port, function () {

    console.log('Immo object service running at port ' + port + '...');
});
