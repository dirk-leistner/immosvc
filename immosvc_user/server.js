var http = require('http');
var util = require('util');
var express = require('express');
var bodyParser = require('body-parser');
var md5 = require('md5');
var port = process.env.PORT || 1337;
var app = express();
var mysql = require('mysql');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var executeDbAction = function (sql) {

    console.log('executeDbAction begin');

    console.log('SQL: ' + sql);

    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'immo_user'
    });

    connection.connect();
    connection.query(sql, function (err, result) {
        if (err) console.log(err.message);
        console.log(result);
    });
    connection.end();

    console.log('executeDbAction end');
};

app.post('/immosvc_user_register', function (req, res) {

    console.log('/immosvc_user_register begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var sessionid = req.body.sessionid;
    var tokenid = req.body.tokenid;
    var usernamevalue = req.body.username;
    var passwordvalue = md5(req.body.password);
    var activationtokenvalue = md5(Math.random().toString());

    var sql = 'INSERT INTO `user` (username, password,status,activationtoken) VALUE (\'' + usernamevalue + '\',\'' + passwordvalue + '\',0,\'' + activationtokenvalue + '\'';

    executeDbAction(sql);

    res.statusCode = 200;
    res.json({ statusCode: 200, statusMessage: 'User successfully registered' });

    console.log('/immosvc_user_register end');
});

app.post('/immosvc_user_refresh_activationtoken', function (req, res) {

    console.log('/immosvc_user_refresh_activationtoken begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var sessionidvalue = req.body.sessionid;
    var tokenidvalue = req.body.tokenid;
    var usernamevalue = req.body.username;
    var activationtokenvalue = md5(Math.random().toString());

    var sql = 'UPDATE `user` SET `activationtoken` = \'' + activationtokenvalue + '\' WHERE `id` > 0 AND status = 0 AND username = \'' + usernamevalue + '\'';

    executeDbAction(sql);

    res.statusCode = 200;
    res.json({ statusCode: 200, statusMessage: 'User activation successfully refreshed' });

    console.log('/immosvc_user_refresh_activationtoken end');
});

app.post('/immosvc_user_activate', function (req, res) {

    console.log('/immosvc_user_activate begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var sessionidvalue = req.body.sessionid;
    var tokenidvalue = req.body.tokenid;
    var usernamevalue = req.body.username;
    var activationtokenvalue = req.body.activationtoken;

    var sql = 'UPDATE `user` SET `status` = 1 WHERE `id` > 0 AND status = 0 AND username = \'' + usernamevalue + '\' AND activationtoken = \'' + activationtokenvalue + '\'';

    executeDbAction(sql);

    res.statusCode = 200;
    res.json({ statusCode: 200, statusMessage: 'User successfully activated' });

    console.log('/immosvc_user_activate end');
});

app.post('/immosvc_user_update', function (req, res) {

    console.log('/immosvc_user_update begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var sessionid = req.body.sessionid;
    var tokenid = req.body.tokenid;
    var usernamevalue = req.body.username;
    var passwordvalue = md5(req.body.password);

    var sql = 'UPDATE `user` SET `password` = \'' + passwordvalue + '\' WHERE `id` > 0 AND username = \'' + usernamevalue + '\'';

    executeDbAction(sql);

    res.statusCode = 200;
    res.json({ statusCode: 200, statusMessage: 'User successfully updated' });

    console.log('/immosvc_user_update end');
});

app.post('/immosvc_user_disable', function (req, res) {

    console.log('/immosvc_user_disable begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var sessionid = req.body.sessionid;
    var tokenid = req.body.tokenid;
    var usernamevalue = req.body.username;

    var sql = 'UPDATE `user` SET `status` = -1 WHERE `id` > 0 AND status = 0 AND username = \'' + usernamevalue + '\'';

    executeDbAction(sql);

    res.statusCode = 200;
    res.json({ statusCode: 200, statusMessage: 'User successfully disabled' });

    console.log('/immosvc_user_disable end');
});

app.post('/immosvc_user_delete', function (req, res) {

    console.log('/immosvc_user_delete begin');

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    var sessionid = req.body.sessionid;
    var tokenid = req.body.tokenid;
    var usernamevalue = req.body.username;

    var sql = 'DELETE FROM `user` WHERE id = \'' + usernamevalue + '\'';

    executeDbAction(sql)

    res.statusCode = 200;
    res.json({ statusCode: 200, statusMessage: 'User successfully deleted' });

    console.log('/immosvc_user_delete end');
});

app.listen(port, function () {

    console.log('Immo user service running...');
});
