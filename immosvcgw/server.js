var http = require('http');
var util = require('util');
var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var port = process.env.PORT || 1337;
var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var executeDbActionCB = function (sql, callback) {

    console.log('executeDbActionCB begin');

    console.log('SQL: ' + sql);

    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'immo_gw'
    });

    connection.connect();
    connection.query(sql, function (err, result) {

        connection.end();

        if (err) callback(err, null);
        else callback(null, result);
        if (err) console.log(err.message);
        
        console.log('DB-Result: ' + JSON.stringify(result));
    });

    console.log('executeDbActionCB end');
}

/*var executeDbAction = function (sql) {

    console.log('executeDbAction begin');

    console.log('SQL: ' + sql);

    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'immo_gw'
    });

    connection.connect();
    let resultdata;
    connection.query(sql, function (err, result) {
        if (err) console.log(err.message);
        connection.end();
        
        resultdata = result;

        console.log('Result: ' + JSON.stringify(result));
        console.log('Resultdata: ' + JSON.stringify(resultdata));
        //return result;
        //return 'testreturn';
    });

    console.log('Resultdata: ' + JSON.stringify(resultdata));

    console.log('executeDbAction end');

    return resultdata;
};*/

app.post('/immosvcgw', function (req, res) {

    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var service = req.body.service;
    var version = req.body.version;
    var method = req.body.method;
    var sessionid = req.body.sessionid;
    var tokenid = req.body.tokenid;
    var data = req.body.data;

    if (service == null || version == null || method == null) {

        console.log('ERROR: Parameter is missing');

        res.statusCode = 500;
        res.json({ statusCode: 500, statusMessage: 'Invalid function call' });

        return;
    }

    console.log('Method: ' + method);
    console.log('Version: ' + version);
    console.log('SessionId: ' + sessionid);
    console.log('TokenId: ' + tokenid);
    console.log('Data: ' + data);

    // get method url
    var sql = 'SELECT `url` AS `url` FROM `method` WHERE `service` = \'' + service + '\' AND `version` = \'' + version + '\' AND `method` = \'' + method + '\' AND `active` = 1';

    executeDbActionCB(sql, function (err, result) {

        if (err) {
            console.log('ERROR: ' + err);

            res.statusCode = 500;
            res.json({ statusCode: 500, statusMessage: 'Invalid function call' });

            return;
        }
        else {
            console.log('RESULT: ' + JSON.stringify(result));

            console.log('Service url: ' + result[0]['url']);

            //res.statusCode = 200;
            //res.json({ statusCode: 200, statusMessage: 'Service gateway called successfully', result: result[0] });

            var request = require('request');
            request.post({

                'headers': { 'content-type': 'application/json' },
                'url': result[0]['url'],
                //'body': data
                'body': JSON.stringify({ 'data': data })

            }, (sub_err, sub_res, sub_body) => {

                if (sub_err) {

                    res.statusCode = 500;
                    res.json({ statusCode: 500, statusMessage: 'Invalid service call', error: sub_err });

                    return console.dir(err);
                }
                else {
                    //console.log(JSON.parse(sub_body));
                    console.log('sub_body: ' + sub_body);
                    console.log('sub_res: ' + JSON.stringify(sub_res));



                    res.statusCode = 200;
                    res.json({ statusCode: 200, statusMessage: 'Service call was successfull', data: sub_res });
                }
                });

            return;
        }
    });

    //console.log('Resultdata: ' + resultdata);

    /*res.statusCode = 200;
    res.json({ statusCode: 200, statusMessage: 'Service gateway called successfully', data: 'test' });*/
});

app.post('/immosvcgwold', function (req, res) {

    res.setHeader('Content-Type', 'application/json; charset=utf-8');

    var method = req.body.method;
    var sessionid = req.body.sessionid;
    var tokenid = req.body.tokenid;

    console.log('Method: ' + method);

    switch (method) {
        case 'login':

            var username = req.body.username;
            var password = req.body.password;

            var request = require('request');
            request.post({
                'headers': { 'content-type': 'application/json' },
                'url': 'http://localhost:1331/immosvc',
                'body': JSON.stringify({
                    'data': data
                })
            }, (sub_err, sub_res, sub_body) => {
                if (sub_err) {
                    return console.dir(err);
                }
                console.dir(JSON.parse(sub_body));

                res.statusCode = 200;
                res.json(JSON.parse(sub_body));
            });
            break;

        default:
            res.statusCode = 500;
            res.json({ statusCode: 500, statusMessage: 'Unknown method' });
            break;
    };
});

app.listen(port, function () {

    console.log('Immo gateway service running at port ' + port + '...');
});
