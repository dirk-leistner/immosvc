var http = require('http');
var util = require('util');
var express = require('express');
var bodyParser = require('body-parser');
var port = process.env.PORT || 1331;
var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/immosvc', function (req, res) {

    var username = req.body.username;
    var password = req.body.password;

    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    res.statusCode = 200;
    res.json({
        statusCode: 200,
        statusMessage: 'Login successful',
        username: username,
        password: password
    });
});

app.listen(port, function () {

    console.log('Immo service running...');
});
